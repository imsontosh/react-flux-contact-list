import { EventEmitter } from 'events';
import _ from 'lodash';

let ListStore = _.extend({}, EventEmitter.prototype, {
    item: [{
        name: 'Person 1',
        id: 0
    }, {
        name: 'Person 2',
        id: 1
    }],

    getItems: function() {
        return this.item;
    },
    addItem: function(item) {
        item.id = this.item.length;
        this.item.push(item);
    },
    removeItem: function(index) {
        this.item.splice(index, 1);
    },
    emitChange: function() {
        this.emit('change');
    },
    addChangeListner: function(callback) {
        this.on('change', callback);
    },
    removeChangeListner: function(callback) {
        this.removeListener('change', callback);
    }
});

export default ListStore;
