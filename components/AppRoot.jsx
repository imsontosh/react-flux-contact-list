// AppRoot.jsx
import React from 'react';
import ListStore from '../stores/ListStore';
import NewItemForm from '../components/NewItemForm';
import AppDispatcher from '../dispatcher/AppDispatcher';

// Method to retrieve state from Stores
let getListState = () => {
    return {
        items: ListStore.getItems()
    };
};

class AppRoot extends React.Component {
    constructor() {
        super();
        this.state = getListState();
    }
    componentDidMount() {
        ListStore.addChangeListner(this._onChange.bind(this));
    }
    componentWillUnmount() {
        ListStore.removeChangeListner(this._onChange.bind(this));
    }
    _onChange() {
        this.setState(getListState());
    }
    removeItem(index) {
        AppDispatcher.dispatch({
            action: 'remove-item',
            new_item: index
        });
    }
    render() {
        let _this = this;
        let itemHtml = this.state.items.map((listItem, index) => {
            return <li key={ listItem.id }>
          { listItem.name }<button onClick={_this.removeItem.bind(this,index)}>X</button>
        </li>;

        });
        return (
            <div>
		        <ul>
		          { itemHtml }
		        </ul>
		        <NewItemForm />
      		</div>
        );
    }

}

export default AppRoot;
