import React from 'react';
import AppDispatcher from '../dispatcher/AppDispatcher';


class NewItemForm extends React.Component {
    constructor() {
        super();
        this.state = {
            inputText: ""
        };
    }
    handleInputChange(event) {
        this.setState({
            inputText: event.target.value
        });
    }
    createItem(e) {
        e.preventDefault();
        let self = this;

        console.log('AppDispatcher', AppDispatcher);

        AppDispatcher.dispatch({
            action: 'add-item',
            new_item: {
                name: self.state.inputText
            }
        });

        this.setState({
            inputText: ""
        });
    }
    render() {
        return (
            <form onSubmit={this.createItem.bind(this)}>
    			<input type="text" onChange={this.handleInputChange.bind(this)} value={this.state.inputText}/>
    			<button>Add New Item</button>
    		</form>
        )
    }
}

export default NewItemForm;
